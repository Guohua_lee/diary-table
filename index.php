<?php 
include_once('config.php');
$dbhandle = mysql_connect($host, $username, $password) 
	or die("Unable to connect to MySQL");

$selected = mysql_select_db($dbname,$dbhandle)
	or die("Could not select database");

$result = mysql_query('SELECT * FROM entry');
$allUser = mysql_query('SELECT id FROM user ORDER BY id');

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>DiaryTable</title>
	<link rel="stylesheet" type="text/css" href="media/css/jquery.dataTables.css">
	<link rel="stylesheet" type="text/css" href="media/css/style.css">
	<link rel="stylesheet" type="text/css" href="media/css/ui-lightness/jquery-ui-1.10.4.custom.min.css">
	
	<script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
	<script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js"></script>
	<script type="text/javascript" language="javascript" src="media/js/jquery-ui-1.10.4.custom.min.js"></script>
	<script type="text/javascript">
		$(function(){
			$('#datepicker').datepicker();
			$('#diary').dataTable();
			$dialog = $('#dialog').dialog({  
  				autoOpen:false,
  				height:400,
  				width:350,   
  				title:"Edit Book",   
  				modal:true,   
  				buttons:[  
   					{text: "Submit", click: function() { $('form',$(this)).submit(); }},  
   					{text: "Cancel", click: function() { $(this).dialog("close"); }},  
  				]  
 			}); 

 			$('#dialog form').submit(function(){
 				var form = $(this);
 				//post form data to form's action attribute
 				$.post('edit.php',$(this).serialize())
 				.done(function(){
 					
 					window.location.href = "index.php";
 				})
 				.fail(function(){
 					if(data.status == 'success'){
 						
 						window.location.href = "index.php";
 					}
 				})
 				.always(function(){
 					window.location.href = "index.php";
 				});

 			});

 			function edit_link_action(){
 				var diary = $(this).closest('.diary');
 				var id = $(this).html();
 				var text = $('.text',diary).html();
 				var userID = $('.user_id',diary).html();
 				var date = $('.date',diary).html().substring(0,10);

 				$('#dialog input[name="id"]').val(id);
 				$('#dialog input[name="text"]').val(text);
 				$('#dialog input[name="userId"]').val(userID);
 				$('#datepicker').datepicker('setDate', date);

 				$dialog.dialog('open');
 			}

 			$('.edit').click(edit_link_action);

		});
	</script>
</head>
<body>
	<div class="container">
	<h1>Diary table</h1>
	<table id="diary" class="display" cellspacing="0" width="100%">
		<thead>
			<tr>
				<td>ID</td>
				<td>Text</td>
				<td>User Id</td>
				<td>Date</td>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<td>ID</td>
				<td>Text</td>
				<td>User Id</td>
				<td>Date</td>
			</tr>
		</tfoot>
		<tbody>
			<?php while ($row = mysql_fetch_array($result)) : 

	?>
			<tr class="diary">
				<td>
					<?php 
					echo ('<a class="edit" href="#">');
					echo htmlspecialchars($row['id']);
					echo ('</a>')?></td>
				<td class="text"><?php echo htmlspecialchars($row['text']) ?></td>
				<td class="user_id"><?php echo htmlspecialchars($row['user_id']) ?></td>
				<td class="date"><?php echo htmlspecialchars($row['date']) ?></td>
			</tr>
			<?php endwhile ?></tbody>
	</table>
	<div id="dialog">
		<form method="post">
			<p>ID:
			<input type="text" name="id"  readonly >
			</p>
			<p>Text:
				<input type="text" name="text">
			</p>
			<p>User:
				<select name="userId">
				<?php while ($userRow = mysql_fetch_array($allUser)){
					echo '<option value='.$userRow['id'].'>'.$userRow['id'].'</option>';
				}
				?>
				</select>
			</p>
			<p>Date:
			<input type="text" name="date" id="datepicker">
			</p>
			
		</form>
	</div>
</div>
</body>
</html>